let
  # This defines the global NUR version
  rev = "45596b1"; # May 21, 2019, 2:33 PM GMT+2
  # Retrieve with `nix-prefetch-url --unpack https://github.com/nix-community/NUR/archive/${rev}.tar.gz`
  sha256 = "0lz26n8nkib55rd8hz171w82l0pcjmdlgilgrsi1s2l4dqkrm065";


  source = rev: sha256: builtins.fetchTarball {
    inherit sha256;
    url = "https://github.com/nix-community/NUR/archive/${rev}.tar.gz";
  };

  nur = rev: sha256: pkgs:
    import (source rev sha256) { inherit pkgs; };
  nurNoPkgs = rev: sha256:
    import (source rev sha256) {};

  global = nur rev sha256;
  globalNoPkgs = nurNoPkgs rev sha256;

in
  {
    inherit nur nurNoPkgs global globalNoPkgs;

    globalRev = rev;
    globalSha256 = sha256;

    globalPackageOverrides = pkgs: { nur = global pkgs; };
  }
