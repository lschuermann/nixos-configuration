let
  gitHubBaseUrl = "https://github.com/nixos/nixpkgs-channels/archive/";
  gitHubExtension = ".tar.gz";

  # It's a good advise to always choose a revision from nixos/nixpkgs-channels
  # as these will be built by hydra.

  globalRev = "40e3191"; # Aug 05, 2019, 8:00 PM GMT+2
  # Retreive with `nix-prefetch-url --unpack https://github.com/nixos/nixpkgs-channels/archive/${rev}.tar.gz`
  globalSha256 = "09pq0yvkzldv88nnfgyzcx96nhs015y61f85z2f5xn9n3sj5rgli";

  fetchNixpkgs = { rev, sha256, baseUrl ? gitHubBaseUrl, extension ? gitHubExtension }:
    builtins.fetchTarball {
      inherit sha256;
      url = "${baseUrl}${rev}${extension}";
    };

in
  rec {
    inherit fetchNixpkgs globalRev globalSha256;

    globalSrc = fetchNixpkgs {rev = globalRev; sha256 = globalSha256; };
    global = import globalSrc {};
  }

