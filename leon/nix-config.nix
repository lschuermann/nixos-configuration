{ pkgs, ... }:

let
  pinpkgs = import ./pinpkgs.nix;
  nur = pinpkgs.nur;

  rev = pinpkgs.defaultRev;
  src = pinpkgs.defaultSrc; 

in
  {
    # Pin nixpkgs to the global default (which is unstable, but that's fine, hopefully)
    nix = {
      useSandbox = true;

      nixPath = [
        "nixpkgs=${src}"
        "nixos-config=/etc/nixos/configuration.nix"
      ];
    };

    # Let's symlink the nixpkgs source so we know what's going on
    system.activationScripts.writeNixpkgsRev = ''
      mkdir -p /run/nixpkgs/
      echo "${rev}" > /run/nixpkgs/rev
      rm -f /run/nixpkgs/src
      ln -sf "${src}" /run/nixpkgs/src
    '';

    # We could include NUR with package overrides, but we can also just import the file
  }
