{ config, pkgs, ... }:
let
  nurModules = (import ../pinpkgs.nix).nurNoPkgs;
in
{
  imports = [ nurModules.repos.lschuermann.modules.files ];

  environment.systemPackages = with pkgs; [ i3 i3status-rust pass ];

  users.extraUsers.leons = {
    files = [ {
      dest = ".config/i3/config";
      content = (import ./i3_config pkgs);
    } {
      dest = ".config/i3/i3status-rs.toml";
      src = ./i3status-rs.toml;
    } {
      dest = ".config/i3/increase_volume";
      src = ./i3_increase_volume;
    } {
      dest = ".config/i3/toggle_touchpad";
      src = ./i3_toggle_touchpad;
    } ];
  };
}
