{ pkgs }:
{
  services.emacs.enable = true;
  services.emacs.defaultEditor = true;
  services.emacs.package = ((pkgs.emacsPackagesNgGen pkgs.emacs).emacsWithPackages (epkgs: [
      epkgs.emacs-libvterm epkgs.pdf-tools
  ]));
}
