{ config, pkgs, ... }:
let
  nurModules = (import ../pinpkgs.nix).nurNoPkgs;
in
{
  imports = [ nurModules.repos.lschuermann.modules.files ];

  environment.systemPackages = [ pkgs.tmux ];

  users.extraUsers.leons.files = [ {
    dest = ".tmux.conf";

    # Could also use a path in the nix store here
    # src = "${pkgs.openssh}/bin/ssh";
    src = ./tmux.conf;

    # Using content works too, bot not together with src
    # content = "...";
  } ];
}
