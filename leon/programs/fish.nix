{ config, pkgs, ... }:
let
  nurModules = (import ../pinpkgs.nix).nurNoPkgs;
in
{
  imports = [ nurModules.repos.lschuermann.modules.files ];

  environment.systemPackages = [ pkgs.fish ];

  programs.fish = {
    enable = true;
  };

  users.extraUsers.leons = {
    shell = pkgs.fish;

    files = [ {
      dest = ".config/fish/config.fish";
      src = ./fish_config.fish;
    } {
      dest = ".config/fish/functions/fish_prompt.fish";
      src = ./fish_prompt.fish;
    } ];
  };
}
