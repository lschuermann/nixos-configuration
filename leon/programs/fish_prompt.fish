function fish_prompt --description 'Write out the prompt'
    set -l nix_shell_info (
      if test "$IN_NIX_SHELL" != ""
        echo -n -s " <> " (set_color red) "nix" (set_color normal)
      end
    )

    set -l color_cwd
    set -l suffix

    switch "$USER"
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set color_cwd $fish_color_cwd
            set suffix '>'
    end

    echo -n -s "$USER" @ (prompt_hostname) ' ' (set_color $color_cwd) (prompt_pwd) (set_color normal) "$nix_shell_info$suffix "
end
