export VISUAL=emacsclient
export EDITOR=emacsclient

function wifi-interface
	ip -o link show | awk -F': ' '{print $2}' | grep wlp | head -n 1
end

function wlink
	iw dev (wifi-interface) link
end

if test -e $HOME/.config/fish/mutable_config.fish
	source $HOME/.config/fish/mutable_config.fish
end
