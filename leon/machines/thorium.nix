# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(6) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix
      ../classes/laptop.nix

      ../services/gui.nix

      # This one imports all of the other, shared wifi networks
      ../services/wireless.nix
    ];

  # Hardware specific options
  hardware.cpu.intel.updateMicrocode = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Encrypted eMMC
  boot.initrd.luks.devices = [
    {
      name = "cryptsd";
      device = "/dev/disk/by-uuid/b9ce3246-b686-4630-b22d-77e83b1a1686";
      preLVM = true;
      allowDiscards = true;
    }
    {
      name = "cryptmmc";
      device = "/dev/disk/by-uuid/1cf0819d-6533-409d-a7ee-cd26c48e9fc6";
      preLVM = true;
      allowDiscards = true;
    }
  ];

  services.printing.enable = true;

  networking = {
    hostName = "thorium";

    firewall = {
      enable = true;
      allowedTCPPorts = lib.mkForce [];
      allowedUDPPorts = lib.mkForce [];
      allowPing = false;
    };
  };

  # Sane settings
  services.openssh.enable = lib.mkForce false;

  # Unleash the full power of the NETBOOK!
  nix.buildCores = 2;
  nix.useSandbox = true;

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";
  services.ntp.enable = true;

  # YubiKey support
  services.pcscd.enable = true;
  hardware.u2f.enable = true;

  environment.systemPackages = with pkgs; [ notmuch offlineimap msmtp yubikey-manager ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
