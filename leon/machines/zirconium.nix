{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../../hardware-configuration.nix

      ../classes/desktop.nix

      ../services/gui.nix
    ];


  # SYSTEM OVERVIEW
  # ASRock Fatal1ty X370 Professional Gaming
  # AMD Ryzen 5 1600X Six-Core Processor @ 3.60GHz
  # - 16 GB DDR4 RAM


  # Use the systemd-boot EFI boot loader.
  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
  
    # Decrypt OS SSD
    initrd.luks.devices = [
      {
        name = "cryptssd";
        device = "/dev/disk/by-uuid/3c418b17-34cd-466b-8aa2-d70577faad7b";
        preLVM = true;
        allowDiscards = true;
      }
    ];

    kernelParams = [ "amd_iommu=on" "video=vesafb:off,efifb:off"  "loglevel=7" "pcie_aspm=off" ];
    consoleLogLevel = 8;

    initrd.kernelModules = [ "kvm-amd" "vfio_virqfd" "vfio_pci" "vfio_iommu_type1" "vfio" ];
    kernelModules = [ "kvm-amd" "vfio_virqfd" "vfio_pci" "vfio_iommu_type1" "vfio" ];

    initrd.preDeviceCommands = ''
      rmmod vfio-pci
      DEVS="0000:2e:00.0 0000:2e:00.1 0000:2f:00.3"

      for DEV in $DEVS; do
        echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
      done
      modprobe -i vfio-pci
    '';
  };


  virtualisation.libvirtd = {
    enable = true;
    qemuOvmf = true;
  };


  environment.systemPackages = with pkgs; [
    virtmanager usbutils pciutils iotop bc cryptsetup gthumb
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  # The nvidia driver is an unfree package
  nixpkgs.config.allowUnfree = true;
  services.xserver.videoDrivers = [ "nvidia" ];

  networking = {
    hostName = "tungsten.is.currently.online";

    # Let's use DHCP for now
    dhcpcd.enable = true;

    firewall = {
      enable = true;
      allowPing = false;
    };

    nameservers = [ "8.8.8.8" "1.1.1.1" ];
    bridges."br-primary".interfaces = [ "enp42s0" ];
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";
  services.chrony.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
