{ server, pkgs, lib, ... }:

{
  imports = [ ./base.nix ];

  networking = lib.mkForce {
    # SANE DEFAULTS!!!
    firewall.enable = true;
    dhcpcd.enable = lib.mkDefault false;
  };

  # Let's just run an openssh server on the default port
  services.openssh = {
    enable = true;

    # We don't want to lock us out accidentally
    openFirewall = true;
  };

  users.extraUsers.root = {
    openssh.authorizedKeys.keys = [
      # Important, should be installed on every system
      # There used to be a backup key here, whose state is currently unknown
    ];
  };
}
