{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # Email
    notmuch offlineimap msmtp muchsync

    # Git
    gitAndTools.gitFull gitAndTools.git-annex git-crypt

    # Other applications
    mumble ffmpeg feh zip unzip file sipcalc libreoffice openssh
    virtmanager virt-viewer ledger hledger vlc bat iw gnome3.nautilus
    nmap telnet bc mtr dino remmina x2goclient usbutils pciutils

    # Corporate stuff
    teams

    # Hardware support
    yubikey-manager
  ];
}
