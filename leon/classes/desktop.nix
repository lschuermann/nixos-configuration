{ config, pkgs, ... }:

{
  imports = [
    ./base.nix

    ../programs/tmux.nix
    ../programs/fish.nix
    ../programs/emacs.nix
    ./user.nix
  ];


  services.pcscd.enable = true;
  hardware.u2f.enable = true;

  services.udisks2.enable = true;

  boot.supportedFilesystems = [ "zfs" ];
}
