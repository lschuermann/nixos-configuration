{ config, pkgs, lib, ... }:

{
  imports = [
    ../nix-config.nix
  ];

  environment.systemPackages = with pkgs; [
    # Packages I can't live without
    wget vim htop git socat gnupg gptfdisk woof gitAndTools.tig minicom picocom
    nload iotop tmux pass gitAndTools.git-crypt # TODO: reptyr unfortunately doesn't build with the latest nixpkgs

    # We need this on most systems
    direnv mosh
  ];

  # Sane sudo config
  # We want to give users in group 'wheel' passwordless sudo
  # and those in group 'sudo' password-enabled sudo
  users.extraGroups = { sudo = {}; };
  security.sudo = {
    enable = true;

    wheelNeedsPassword = false;
    extraRules = [
      { groups = [ "sudo" ]; commands = [ "ALL" ]; }
    ];
  };

  # Enable manpage support
  documentation.man.enable = true;

  # Show the nixos manual on tty 8
  documentation.nixos.enable = true;
  services.nixosManual = {
    showManual = true;
    ttyNumber = 8;
  };

  # Disable _any_ password-based login with OpenSSH
  services.openssh = lib.mkForce {
    challengeResponseAuthentication = false;
    passwordAuthentication = false;
    permitRootLogin = "prohibit-password";
  };
}
