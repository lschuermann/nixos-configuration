{ config, pkgs, ... }:

{
  imports = [
    ./base.nix

    ../programs/tmux.nix
    ../programs/fish.nix
    ./user.nix
  ];

  environment.systemPackages = with pkgs; [
    emacs
  ];

  services.xserver.libinput = {
    enable = true;
    tapping = false;
  };

  services.pcscd.enable = true;
  hardware.u2f.enable = true;

  services.udisks2.enable = true;

  boot.supportedFilesystems = [ "zfs" ];
}
