# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ ../programs/i3.nix
    ];

  environment.systemPackages = with pkgs; [
    chromium firefox xfce.terminal arandr scrot acpilight xorg.xev pavucontrol alsaTools
    evince xournal xournalpp
  ];
  sound.enable = true;

  fonts.fonts = with pkgs; [ font-awesome-ttf fantasque-sans-mono ];

  services.physlock = {
    enable = true;
    allowAnyUser = true;
    disableSysRq = true;
  };
  security.sudo.extraRules = [ {
    commands = [
      { command = "${pkgs.systemd}/bin/systemctl start physlock"; options = [ "NOPASSWD" ]; }
      { command = "${pkgs.acpilight}/bin/xbacklight"; options = [ "NOPASSWD" ]; }
    ];
    users = [ "leons" ];
  } ];

  services.logind = {
    lidSwitch = "suspend";

    extraConfig = ''
      HandlePowerKey=ignore
    '';
  };

  #systemd.services.lock-mute = {
  #  description = "Mute the audio before suspend";
  #  serviceConfig = {
  #    ExecStart = ''${pkgs.busybox}/bin/sh -c "${config.hardware.pulseaudio.package.out}/bin/pactl set-sink-mute @DEFAULT_SINK@ 1 || true"'';
  #    Type = "oneshot";
  #    User = "leons";
  #  };
  #  wantedBy = [ "sleep.target" "suspend.target" ];
  #  before = [ "lock-suspend.service" "sleep.target" "suspend.target" ];
  #  enable = true;
  #};

  # Enable CUPS to print documents.
  services.printing.enable = true;

  hardware.pulseaudio.enable = true;

  services.xserver = {
    enable = true;

    layout = "us";
    xkbVariant = "mac";
    xkbOptions = "ctrl:swapcaps";

    libinput.enable = true;

    displayManager.lightdm = {
      enable = true;
    };

    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [ dmenu i3status-rust ];
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.leons = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "sudo" "dialout" ];
  };
}
