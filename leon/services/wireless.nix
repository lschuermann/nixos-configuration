{ config, pkgs, lib, ... }:
let

  decrypt = ((import ../../lib/openssl_decrypt.nix) pkgs) ../../secrets/wireless_secret;

  toWifiAttrSet = wifiList:
    builtins.listToAttrs (map (w:
      {
        name = w.ssid;
        value =
          (lib.filterAttrs (n: _: n != "ssid" && n != "priority") w) //
          {
            priority = if (w ? priority) && w.priority != null then w.priority else 50;
          };
      }
    ) wifiList);

  encPsk = comment: priority: ssid: psk_ciphertext: {
    ssid = ssid;
    psk = decrypt "${comment}_psk" psk_ciphertext;
    priority = priority;
  };

  encWifi = comment: priority: ssid_ciphertext: psk_ciphertext:
    (encPsk
      comment
      priority
      (decrypt "${comment}_ssid" ssid_ciphertext)
      psk_ciphertext
    );
in
{
  networking.wireless = {
    enable = true;
    networks = toWifiAttrSet [

      # ----- PUBLIC -----
      { ssid = "WIFIonICE"; priority = 40; }
      { ssid = "Freifunk"; priority = 40; }

      # ----- PRIMARY -----
      (encWifi
        "home_0" 80
        "U2FsdGVkX19RC98Nl8FkhiLSs9BAkYg0bicZFNIVFcgXmXY3rHfsDVu1fv24OUIJ"
        "U2FsdGVkX1+ngWNz3tPAgGmz5N84lv62EgBtAYAqHOiRkDmjDiDNbLyBDv0kkBbp")
      (encWifi
        "home_1_wifi_legacy" 80
        "U2FsdGVkX1/dI8JdyE3PFJ3dBfOc1oJQd3KRClrOUmMhFhbDZCjQF1+Ls2ZMojkG"
        "U2FsdGVkX1+yHbKaeiTX6ZSG7zkG219JNjkk/nmLrlnN01U3utimtbAydLHniB41")
      (encWifi
        "home_1_wifi" 81
        "U2FsdGVkX1+gDZWUm+ZdczjFnStA9do32TXwP0bfyuwJOzjWLcpYnVTgDX9l24Ce"
        "U2FsdGVkX1+yHbKaeiTX6ZSG7zkG219JNjkk/nmLrlnN01U3utimtbAydLHniB41")

      # ----- OTHER PLACES -----
      (encWifi
        "hackerspace_siegen" 70
        "U2FsdGVkX19GuOIJHbAWyXsbXFROEYoUCimniTwzWX7VLFXjr9Pqcr25V+Ykmbu1"
        "U2FsdGVkX18mNUl3+mvb1zrGDgL9gTkjP3K9qje0OwA=")
      (encWifi
        "hackerspace_stuttgart" 70
        "U2FsdGVkX1+043FaBOf5RXF5JAJXRJ26PHD5tZHfjPo="
        "U2FsdGVkX19ub5yHOGjS3rTAmTUJbLR7xYxF5G3xraU=")

      # University Wifi
      { ssid = "eduroam";
        priority = 70;
        auth = ''
          key_mgmt=WPA-EAP
          pairwise=CCMP
          group=CCMP TKIP
          eap=PEAP
          domain_suffix_match="radius.dhbw-stuttgart.de"
          phase2="auth=MSCHAPV2"
          ca_cert="${
            pkgs.writeText "eduroam_certificates.pem" (
              (builtins.readFile ./eduroam_certificates.pem) +
              (builtins.readFile (builtins.fetchurl {
                url = "https://www.pki.dfn.de/fileadmin/PKI/zertifikate/deutsche-telekom-root-ca-2.pem";
                sha256 = "0b64wvs01a9mcv6b164aywiqs0cirnpffb4p618532s49p41j3vk";
              }))
            )
          }"
          anonymous_identity="31e3b343e2f6ec97af4da5978bc62f73@lehre.dhbw-stuttgart.de"
          identity="${decrypt "eduroam_username"
            "U2FsdGVkX18O8C3UKb3tLcRxTkYO7aMab02ocRdAkXG5xKHfm7ZWN6XJR7WeseEvizPAkbXicCyQiLTuvfYPtg=="}"
          password="${decrypt "eduroam_passphrase"
            "U2FsdGVkX1+0z0VFjCdadwSNo9E3KrgYyBZna80QvXBzZinNEUmokwNfRy3SpDvYeEUl2aE4/ttnI3ZJLfVIfQ=="}"
        '';
      }

      # Company 0 employee network
      { ssid = decrypt "company0_employee_ssid" "U2FsdGVkX1+YoJFbtL7k9zxGFXAD/t+LdmLCfasQaXY=";
        priority = 60;
        auth = ''
          key_mgmt=WPA-EAP
          identity="${decrypt "company0_employee_username"
            "U2FsdGVkX1+KPXEz+0dqA+3kZ224CHGpldTKxRryQF2yl1XFFuCjuKGrWq8IOket"}"
          password="${decrypt "company0_employee_passphrase" "U2FsdGVkX1/amufb126+TIOYBSCdOAz+HoaPedbqvHY="}"
          eap=PEAP
          phase1="peaplabel=0"
          phase2="auth=MSCHAPV2"
        '';
      }
          eap=PEAP
          phase1="peaplabel=0"
          phase2="auth=MSCHAPV2"
        '';
      }

      # Phone Hotspot (In case nothing else is availabe)
      (encWifi
        "phone_hotspot" 20
        "U2FsdGVkX1/+SelfBH831ocyldNX2mWPBr6dMCvP5lA="
        "U2FsdGVkX18ZFt/MtVD5ltwhGs0pobXq34w0J9oKpiE=")

    ];
  };
}
