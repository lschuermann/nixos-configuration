let
  pinpkgs = import ../shared/pinpkgs.nix;
  nurFn = (import ../shared/nur.nix).global;
  nurNoPkgs = (import ../shared/nur.nix).globalNoPkgs;

  defaultRev = pinpkgs.globalRev;
  defaultSha256 = pinpkgs.globalSha256;


in
  rec {
    inherit (pinpkgs) fetchNixpkgs;

    defaultRev = "8ba41a1";
    defaultSha256 = "sha256:0c2wn7si8vcx0yqwm92dpry8zqjglj9dfrvmww6ha6ihnjl6mfhh";

    defaultSrc = pinpkgs.fetchNixpkgs {
      baseUrl = "https://github.com/nixos/nixpkgs/archive/";
      rev = defaultRev;
      sha256 = defaultSha256;
    };
    default = import defaultSrc {};

    inherit nurNoPkgs;
    nur = nurFn default;
  }
