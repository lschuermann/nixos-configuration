{ lib, ... }:
  {
    imports = [
      ./altFirewallModule.nix
    ];

    networking.firewall.enable = false;
    networking.altFirewall.enable = true;
  }
