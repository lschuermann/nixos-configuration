{ ...  }:

{
  networking = {
    bridges."br-luxemburg".interfaces = [ ];
    interfaces."br-luxemburg" = {
      ipv4.addresses = [ { address = "178.63.44.182"; prefixLength = 32; } ];
      ipv4.routes = [ { address = "178.63.44.178"; prefixLength = 32; } ];

      ipv6.addresses = [ { address = "2a01:4f8:120:614b:dae::1"; prefixLength = 128; } ];
      ipv6.routes = [
        { address = "2a01:4f8:120:614b:dae::2"; prefixLength = 128; }
        { address = "2a01:4f8:120:614b:dae::"; prefixLength = 80; via = "2a01:4f8:120:614b:dae::2"; }
      ];
    };
  };

  services.virtualMachines."daniel-luxemburg" = {
    enable = true;
    cores = 4;
    memory = 4096;

    volumes = {
      "sda" = {
        path = "/dev/vg0/daniel_luxemburg_0_0";
        type = "block";
        bus = "virtio";
      };
      "sdb" = {
        path = "/dev/vg1/daniel_luxemburg_1_0";
        type = "block";
        bus = "virtio";
      };
      "sdc" = {
        path = "/dev/vg0/daniel_luxemburg_0_1";
        type = "block";
        bus = "virtio";
      };
      "sdd" = {
        path = "/dev/vg1/daniel_luxemburg_1_1";
        type = "block";
        bus = "virtio";
      };
    };

    nics = {
      "00:16:3e:20:5b:d7" = {
        type = "bridge";
        bridge = "br-luxemburg";
      };
    };

    uefi = true;
    cd = { path = "/root/debian-10.0.0-amd64-netinst.iso"; boot = false; };
  };
}
