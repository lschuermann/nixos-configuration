{ ...  }:

{
  networking = {
    bridges."br-carbon".interfaces = [ ];
    interfaces."br-carbon" = {
      ipv4.addresses = [ { address = "10.163.0.1"; prefixLength = 30; } ];
      ipv6.addresses = [ { address = "2a01:4f8:120:614b:1e0::1"; prefixLength = 128; } ];
      ipv6.routes = [
        { address = "2a01:4f8:120:614b:1e0::2"; prefixLength = 128; }
        { address = "2a01:4f8:120:614b:1e0::"; prefixLength = 80; via = "2a01:4f8:120:614b:1e0::2"; }
      ];
    };
  };

  services.virtualMachines."leon-carbon" = {
    enable = true;
    cores = 4;
    memory = 4096;

    volumes = {
      "sda" = {
        path = "/dev/vg0/leon-carbon0";
        type = "block";
        bus = "virtio";
      };
      "sdb" = {
        path = "/dev/vg1/leon-carbon1";
        type = "block";
        bus = "virtio";
      };
    };

    nics = {
      "de:b9:ce:2d:ee:56" = {
        type = "bridge";
        bridge = "br-carbon";
      };
    };

    uefi = true;
    cd = { path = "/root/nixos-minimal-18.09.2455.3eed6d45739-x86_64-linux.iso"; boot = false; };
  };
}
