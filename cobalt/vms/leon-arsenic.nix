{ ...  }:

{
  networking = {
    bridges."br-arsenic".interfaces = [ ];
    interfaces."br-arsenic" = {
      ipv4.addresses = [ { address = "178.63.44.182"; prefixLength = 32; } ];
      ipv4.routes = [ { address = "178.63.44.188"; prefixLength = 32; } ];

      ipv6.addresses = [ { address = "2a01:4f8:120:614b:1234::1"; prefixLength = 128; } ];
      ipv6.routes = [
        { address = "2a01:4f8:120:614b:1234::2"; prefixLength = 128; }
        { address = "2a01:4f8:120:614b:1234::"; prefixLength = 80; via = "2a01:4f8:120:614b:1234::2"; }
      ];
    };
  };

  services.virtualMachines."leon-arsenic" = {
    enable = true;
    cores = 4;
    memory = 4096;

    volumes = {
      "sda" = {
        path = "/dev/vg0/leon-arsenic0";
        type = "block";
        bus = "virtio";
      };
      "sdb" = {
        path = "/dev/vg1/leon-arsenic1";
        type = "block";
        bus = "virtio";
      };
    };

    nics = {
      "f6:7a:5d:6a:ef:c7" = {
        type = "bridge";
        bridge = "br-arsenic";
      };
    };

    uefi = true;
    cd = { path = "/root/nixos-minimal-18.09.2455.3eed6d45739-x86_64-linux.iso"; boot = false; };
  };
}
