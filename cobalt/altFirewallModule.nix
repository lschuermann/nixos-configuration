{ config, lib, ... }:

with lib;

let
  cfg = config.networking.altFirewall;
in
{
  options = {
    networking.altFirewall = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Whether to enable the alternative nftables firewall configuration";
      };

      inputRules = mkOption {
        type = with types; listOf string;
        default = [ ];
        description = "List of input rules to insert into the nftables configuration";
      };
      forwardRules = mkOption {
        type = with types; listOf string;
        default = [ ];
        description = "List of output rules to insert into the nftables configuration";
      };
      outputRules = mkOption {
        type = with types; listOf string;
        default = [ ];
        description = "List of forward rules to insert into the nftables configuration";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    networking.nftables.enable = true;
    networking.nftables.ruleset = ''
      # firewall
      table inet filter {
        chain input {
          type filter hook input priority 0; policy drop;
          ct state invalid counter drop comment "drop invalid packets"
          ct state {established, related} counter accept comment "accept all connections related to connections made by us"

          iifname lo accept comment "Loopback interface"

          ip protocol icmp counter accept comment "Accept all host ICMP"
          ip6 nexthdr icmpv6 counter accept comment "Accept all host ICMP"

          tcp dport { ${ builtins.concatStringsSep ", " (builtins.map (p: builtins.toString p) config.services.openssh.ports) } } counter accept comment "Host sshd"
          ip6 saddr 2a01:4f8:120:614b:0:109:0:2 tcp dport 61402 counter accept comment "Prometheus node exporter"

          counter reject with icmpx type admin-prohibited comment "Dropped packets"
        }
      
        chain forward {
          type filter hook forward priority 0; policy drop;
          ct state invalid counter drop comment "drop invalid forward packets"
          ct state {established, related} counter accept comment "forward related and established connections"

          # REVERSE PROXY INCOMING
          ip6 daddr 2a01:4f8:120:614b:0:abc:0:2 tcp dport {80, 443, 8448} counter accept comment "Reverse proxy http(s)"
          ip daddr 10.162.0.2 tcp dport {80, 443, 8448} counter accept comment "Reverse proxy http(s)"
          ip6 saddr 2a01:4f8:120:614b:0:109:0:2 ip6 daddr 2a01:4f8:120:614b:0:abc:0:2 tcp dport 4040 counter accept comment "Reverse proxy monitoring - prometheus exporter"

          # MONITORING
          ip6 saddr 2a01:4f8:120:614b:0:abc:0:2 ip6 daddr 2a01:4f8:120:614b:0:109:0:2 tcp dport 61401 counter accept comment "Reverse proxy grafana access"

          # MATRIX
          ip6 saddr 2a01:4f8:120:614b:0:abc:0:2 ip6 daddr 2a01:4f8:120:614b:0:3a1::2 tcp dport 8080 counter accept comment "Reverse proxy access to matrix"
          ip6 daddr 2a01:4f8:120:614b:0:3a1::2 tcp dport 53000-53999 counter accept comment "corturn server for matrix voip"
          ip daddr 10.162.1.2 tcp dport 53000-53999 counter accept comment "corturn server for matrix voip nat"
          ip6 daddr 2a01:4f8:120:614b:0:3a1::2 udp dport 53000-53999 counter accept comment "corturn server for matrix voip"
          ip daddr 10.162.1.2 udp dport 53000-53999 counter accept comment "corturn server for matrix voip nat"

          # GITOLITE
          ip6 daddr 2a01:4f8:120:614b:0:917:0:2 tcp dport {22, 9418} counter accept comment "gitolite ssh, git"
          ip daddr 10.162.3.2 tcp dport {22, 9418} counter accept comment "gitolite ssh, git"
          ip saddr 10.162.0.2 ip daddr 10.162.3.2 tcp dport 80 counter accept comment "Reverse proxy access to cgit"

          # NEXTCLOUD
          ip6 saddr 2a01:4f8:120:614b:0:abc:0:2 ip6 daddr 2a01:4f8:120:614b:0:c10:0:2 tcp dport 80 counter accept comment "Reverse proxy access to nextcloud"

          # WEKAN
          ip saddr 10.162.0.2 ip daddr 10.162.4.2 tcp dport 3000 counter accept comment "Reverse proxy wekan access"

          # LEON-CARBON
          ip6 daddr 2a01:4f8:120:614b:1e0::/80 counter accept comment "forward incoming packets to leon-carbon VM (firewall inside VM)"
          ip daddr 10.163.0.2 counter accept comment "forward incoming packets to leon-carbon VM (firewall inside VM)"

          # LEON-ARSENIC
          ip6 daddr 2a01:4f8:120:614b:1234::/80 counter accept comment "forward incoming packets to leon-arsenic VM (firewall inside VM)"
          ip daddr 178.63.44.188 counter accept comment "forward incoming packets to leon-arsenic VM (firewall inside VM)"

          # DANIEL-LUXEMBURG
          ip6 daddr 2a01:4f8:120:614b:dae::/80 counter accept comment "forward incoming packets to daniel-luxemburg VM (firewall inside VM)"
          ip daddr 178.63.44.178 counter accept comment "forward incoming packets to daniel-luxemburg VM (firewall inside VM)"
          
          # SOURCEDS
          ip6 daddr 2a01:4f8:120:614b:c0d::/80 counter tcp dport 27015 accept comment "Allow Game Transmission port"
          ip6 daddr 2a01:4f8:120:614b:c0d::/80 counter udp dport { 27015, 27020, 27005, 26900 } accept comment "Allow all other strange SourceDS ports"
          ip6 saddr 2a01:4f8:120:614b:0:109:0:2 ip6 daddr 2a01:4f8:120:614b:c0d::/80 counter tcp dport 9137 accept comment "Allow prometheus exporter access"
          ip daddr 10.163.5.2 counter tcp dport 27015 accept comment "Allow Game Transmission port"
          ip daddr 10.163.5.2 counter udp dport { 27015, 27020, 27005, 26900 } accept comment "Allow all other strange SourceDS ports"

          # sourceds should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname br-leuschner ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for SteamCMD"
          iifname br-leuschner ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for SteamCMD"
          iifname br-leuschner counter accept comment "forward outgoing packets from SteamCMD container (for downloads)"
          
          # gitolite should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname ve-gitolite ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for gitolite"
          iifname ve-gitolite ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for gitolite"
          iifname ve-gitolite counter accept comment "forward outgoing packets from gitolite container (for mirroring, non-local access)"

          # nextcloud should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname ve-nextcloud ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for nextcloud"
          iifname ve-nextcloud ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for nextcloud"
          iifname ve-nextcloud counter accept comment "forward outgoing packets from nextcloud container (for federation, plugins, non-local access)"

          # leon-carbon should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname br-carbon ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for leon carbon-vm"
          iifname br-carbon ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for leon carbon-vm"
          iifname br-carbon counter accept comment "forward outgoing packets from leon carbon-vm (non-local access)"

          # leon-arsenic should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname br-arsenic ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for leon arsenic-vm"
          # We don't need to protect the internal network, as it has a public IP anyways and no internal
          iifname br-arsenic counter accept comment "forward outgoing packets from leon arsenic-vm (non-local access)"
          
          # daniel-luxemburg should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname br-luxemburg ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for daniel luxemburg-vm"
          iifname br-luxemburg ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for daniel luxemburg-vm"
          iifname br-luxemburg counter accept comment "forward outgoing packets from daniel luxemburg-vm (non-local access)"
          
          # matrix should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname ve-matrix ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for matrix"
          iifname ve-matrix ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for matrix"
          iifname ve-matrix counter accept comment "forward outgoing packets from matrix container (for federation, non-local access)"
          
          # revproxy should have access to the internet in general, but only to specific local resources
          # therefore we first have to accept specific local resources, then reject all other local resources and
          # finally accept all other traffic (to the internet)
          iifname ve-revproxy ip6 daddr 2a01:4f8:120:614b::/64 counter reject with icmpv6 type no-route comment "Deny local access for revproxy"
          iifname ve-revproxy ip daddr 10.0.0.0/8 counter reject with icmp type net-unreachable comment "Deny local access for revproxy"
          iifname ve-revproxy counter accept comment "forward outgoing packets from revproxy container (for federation, non-local access)"

	  # wekan needs access to alkaid.uberspace.de in order to send forgot password mails
          iifname ve-wekan ip6 daddr 2a01:4f8:0:1::add:1010/128 udp dport 53 counter accept comment "allow DNS access for wekan"
	  iifname ve-wekan ip6 daddr 2a00:d0c0:200:0:b9:1a:9c:2c/128 tcp dport 587 counter accept comment "allow submission access to alkaid.uberspace.de"
          iifname ve-wekan ip daddr 185.26.156.44/32 tcp dport 587 counter accept comment "allow submission access to alkaid.uberspace.de"

          counter reject with icmpx type no-route comment "Dropped packets"
        }
        
        chain output {
          type filter hook output priority 100; policy accept;
        }
      }

      table ip nat {
        chain prerouting {
          type nat hook prerouting priority 0; policy accept;
          ip daddr 178.63.44.182 tcp dport {80, 443, 8448} counter dnat 10.162.0.2 comment "reverse proxy http(s)"
          ip daddr 178.63.44.182 tcp dport {22, 9418} counter dnat 10.162.3.2 comment "gitolite ssh"

          ip daddr 178.63.44.182 tcp dport 62842 counter dnat 10.163.0.2 comment "leon VM ssh"
          ip daddr 178.63.44.182 tcp dport 62847 counter dnat 10.163.0.2 comment "leon VM ssh"
          ip daddr 178.63.44.182 tcp dport 25565 counter dnat 10.163.0.2 comment "leon VM minecraft"
          ip daddr 178.63.44.182 tcp dport 62843 counter dnat 10.163.0.2 comment "leon VM xrdp"
          ip daddr 178.63.44.182 tcp dport 62846 counter dnat 10.163.0.2 comment "leon VM ssh platinum v4 tunnel"
          ip daddr 178.63.44.182 udp dport {62844, 62845} counter dnat 10.163.0.2 comment "leon VM mosh"
          ip daddr 178.63.44.182 udp dport 62891 counter dnat 10.163.0.2 comment "leon VM lvpn wg"

          ip daddr 178.63.44.182 tcp dport 53000-53999 counter dnat 10.162.1.2 comment "matrix coturn server ipv4 access"
          ip daddr 178.63.44.182 udp dport 53000-53999 counter dnat 10.162.1.2 comment "matrix coturn server ipv4 access"
        }

        chain postrouting {
          type nat hook postrouting priority 100; policy accept;

          # Do not NAT public IPs.
          ip saddr { 178.63.44.188, 178.63.44.178 } accept

          # But do NAT everything else.
          oif enp2s0 counter masquerade
        }
      }
    '';

    boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = 1;
    boot.kernel.sysctl."net.ipv4.ip_forward" = 1;
  };
}
