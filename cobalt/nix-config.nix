{ pkgs, ... }:

let
  pinpkgs = import ../shared/pinpkgs.nix;
  nur = (import ../shared/nur.nix).global pkgs;

in
  {
    # Pin nixpkgs to the global default (which is unstable, but that's fine, hopefully)
    nix = {
      useSandbox = true;
  
      nixPath = [
        "nixpkgs=${pinpkgs.globalSrc}"
        "nixos-config=/etc/nixos/configuration.nix"
      ];
    };
 
    # Let's symlink the nixpkgs source so we know what's going on 
    system.activationScripts.writeNixpkgsRev = ''
      mkdir -p /run/nixpkgs/
      echo "${pinpkgs.globalRev}" > /run/nixpkgs/rev
      rm -f /run/nixpkgs/src
      ln -sf "${pinpkgs.globalSrc}" /run/nixpkgs/src
    '';


   # We could include NUR with package overrides, but we can also just import the file
   
  
    # Enable manpage support
    documentation.man.enable = true;
  }
