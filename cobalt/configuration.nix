{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ../hardware-configuration.nix

      ./nix-config.nix
      ./firewall.nix

      ./services/prometheusHostExporter.nix

      # Container services
      ./services/revproxy.nix
      ./services/prometheus.nix
      ./services/matrix.nix
      ./services/nextcloud.nix
      ./services/wekan.nix
      ./services/gitolite.nix

      # VM module
      ../lib/vm.nix

      # Leon's personal VM
      ./vms/leon.nix
      # Leon's production VM
      ./vms/leon-arsenic.nix

      # Daniel's luxemburg mail VM
      ./vms/luxemburg.nix
    ];


  # SYSTEM OVERVIEW
  # Hetzner Online GmbH SB24
  # Intel(R) Core(TM) CPU i7-4770 QuadCore @ 3.40 - 3.90GHz
  # - 32 GB DDR3 RAM
  # - 2TB ST2000NM0033-9ZM175 (sda)
  # - 2TB WDC WD2000FYYZ-01UL1B1 (sdb)
  # - RealTek RTL-8169 Gigabit Ethernet adapter

  # Hardware specific options
  nix.buildCores = 8;
  hardware.cpu.intel.updateMicrocode = true;

  # Hetzner only supports BIOS-boot systems
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";        # TODO: We should use RAID-1 for GRUB, but it doesn't like it
  };

  boot.kernelParams = [
    # Block module load in kernel parameters, as boot.blacklist is only relevant to udev
    "module_blacklist=ip_tables,ip6_tables"

    # Nested KVM virtualisation support
    "kvm-intel.nested=1"
  ];

  # Each hard drive stores the other one's encryption key. This way, to read data both drives are
  # required. This also means, that we can't boot if one fails. BACKUP THE KEYS!
  boot.initrd.luks.devices = [ {
    name = "vg0pv0";
    device = "/dev/disk/by-uuid/79021cf7-9b0c-4b86-a149-6f4a685c328d";      # sda4
    keyFile = "/dev/disk/by-partuuid/879791f8-4918-4a6a-8e9b-588a42663856"; # sdb3
    allowDiscards = true;
    preLVM = true;
  } {
    name = "vg1pv0";
    device = "/dev/disk/by-uuid/005b3062-e16b-436e-9d6c-16e074fedb2a";      # sdb4
    keyFile = "/dev/disk/by-partuuid/97ca5ca6-fda6-412e-8540-d9604978e7aa"; # sda3
    allowDiscards = true;
    preLVM = true;
  } ];

  networking = {
    hostName = "cobalt.is.currently.online";

    interfaces.enp2s0 = {
      ipv4.addresses = [ { address = "178.63.44.182"; prefixLength = 26; } ];
      ipv6.addresses = [ { address = "2a01:4f8:120:614b::2"; prefixLength = 64; } ];
    };

    defaultGateway = "178.63.44.129";
    defaultGateway6 = { address = "fe80::1"; interface = "enp2s0"; };
    nameservers = [ "2a01:4f8:0:1::add:1010" "2606:4700:4700::1111" "2606:4700:4700::1001" "1.1.1.1" "1.0.0.1" ];
    timeServers = [ "ntp1.hetzner.de" "ntp2.hetzner.com" "ntp3.hetzner.net" ];

  };

  services.openssh = {
    enable = true;
    ports = [ 2222 ];
  };

  users.extraUsers.root = {
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCg3EfkA5xx9Nl19vDQ3+H2K+5Kqs9aeyliqrneVTg6LT2cWcyaJjCxXLPYObumSRX3fkOaCwsGEqwsl9wFsj+NmUC6zwuQkvmDJUWVV6v6iNWh8FjJzDvOc340r45NEG/+QjoVBbVn5auX19Dcd4vhiGMCmoMPoKJLCUIBCpCbmdf8mqyenDGQdPlZmZ4QtFO5fw/JCSrOL7CEoC9MZp091vv+DiUOqL94lzUXepZmzB94qQf8/EePsNP3L9tsyh7PCEnGPGgOnnlhbFym8y6NfzvjTjRxdrudshAbl6wokBUjEmQAAxYsVK8PaHoF4EbPlvCufiC/ncu54ig+SecSq559r5bvQh+0JEqBRJRRIecUuxQl+LI2VZXpxaJDJ/FbQCFwTvO+OciUvdZjDGSpJS/NuwxGMsc1Sffs+LKjLiA9n+pazAAM/hfyBz61SwmgGrjcrym+JpzauikKhtwrTTqyYC7zHix0hIvXMtpEc10RrCWwxIqWfRPrziN/PzTeKFHlhk+crvt7hRfSRuxCrUarPL2ZG1b4Bu/Ko8GLDWMphtWzzvo4XRLrFaAre4YSgREjv5MTwBsVsAMnnObmsPK8acmc2j9llU/KHP6n0NzC4rsBrLwH9ZCtrgCr7rf8nDCxyUJLQp11rAeFi1WauOTMeTMoUenwJV9qX+chww== Daniel - cardno:000609039286"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9XgOJNVpO00+p04GwoL0viFco4p24PrsPzvsCOiAETJ6ttYiOFjYrpjQvNLsDse0PcW+duhtjyyp+Y7JZaOTeG6FX1Y8j6gDCUML58f4NlKtnWzfMftYkVO8QeYzdjJhG3J2zBXuqMmen7elVgZouvM/H47X620q7BssTTS1YIVnXxNn5jip8UbSJ1073MnUuTjSGrmS9yyLQx4Ka9/u6zzPDASw6OWXyzoXkWVpU5VzAqYk8Ob9C+lFQw5LMERsQBADoNB+kg/m+OoKS7XXu9+WFdTKsqhy7/c6hijRPaNsP/JRfsxEyjF+Y8LyokU2OXq0MWM0xZrt8O3/DsXAKqyIrGCVPQX+eeXvqP8LusFm2CDe8zoUeyLXvBdZX6Xxyy00OHHQRsuIbBYCJOUMbmDwIcEaC4DELjcNFZXJQYhj2hvB2sQvTMjnS58FDkD+IgVTTlTTzkEiDwbIV6lHbSqzA6AvZtZXd1+rQ4wFIT0clQhAGpBWKN+8Cotl51yC+P+nbU9xlCXM17Q3Pev2sjZFx1VU3oa5SEzEv74aWhUDCaATun/ebi/1Scm3ekOsiPVHuUBm9a2GUMYIuAOTL6W9AM2zZJ8xWkJw5Rj1eQnIDwD9izCRR1gJXlkTYwLn6ZnLawX6FS6Vvj5NV8mjjU4xlhnwaWF0b/Jmm3E966w== Leon - openpgp:0xEAF68EC5 -- YubiKey 5 Authentication Key"
      "ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBHpq+Zu8kLgYQ/zONeIEWwNvePF56lSAgx321d++gzfu6cO7luZJyeSyfXd1A+RcetiPn8TAMazi5+bTs9PznvHEvpMtvCAd58XUvP/P0Gmrj4Uk9uBDAAa8LYWj4GW1Vg== Leon - openpgp:0xF4AC5B95 -- GnuPG Smartcard V3.3 0005 00007D5B"
    ];
  };

  environment.systemPackages = with pkgs; [
    wget vim tmux htop nload git gnupg
  ];

  system.activationScripts.setCorrectPermissions = ''
    # Set correct application data permissions
    echo "Setting correct filesystem permissions"
    chown root:root /media/application-data
    chmod 500 /media/application-data
  '';

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";
  services.chrony.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?

}
