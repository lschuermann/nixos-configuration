{ ... }:

{
  containers.nextcloud = {
    autoStart = true;
    privateNetwork = true;

    hostAddress = "10.162.2.1";
    localAddress = "10.162.2.2";
    hostAddress6 = "2a01:4f8:120:614b:0:c10::1";
    localAddress6 = "2a01:4f8:120:614b:0:c10::2";

    bindMounts."/data" = {
      hostPath = "/media/application-data/nextcloud";
      isReadOnly = false;
    };

    config = { pkgs, lib, ... }: {

      networking.firewall.enable = false;

      environment.systemPackages = [ pkgs.borgbackup ];
      systemd.services."borgbackup-job-nextcloud" = {
        serviceConfig = {
          ProtectSystem = lib.mkForce null;
          PrivateTmp = lib.mkForce null;
          IOSchedulingClass = lib.mkForce null;
          CPUSchedulingPolicy = lib.mkForce null;
          ReadWritePaths = lib.mkForce null;
        };
      };
      services.borgbackup.jobs."nextcloud" = {
        paths = [ "/data/nextcloud-home" "/tmp/backup-database-dump" ];
        repo = "platinum-backup:.";
        archiveBaseName = "cobalt-nextcloud";
        startAt = "*-*-* 02:00:00"; # TODO: Randomize
        user = "root";
        group = "root";
        encryption = {
          mode = "repokey";
          passCommand = "cat /data/backup_passphrase";
        };
        compression = "auto,lz4";
        readWritePaths = [ "/tmp/backup-database-dump" ];
        privateTmp = false;
        doInit = false;
        preHook = ''
          echo "Setting matinenance mode"
          /run/current-system/sw/bin/nextcloud-occ maintenance:mode --on

          echo "Dumping postgresql database"
          rm -rf /tmp/backup-database-dump
          mkdir -p /tmp/backup-database-dump
          ${pkgs.sudo}/bin/sudo -u postgres ${pkgs.postgresql}/bin/pg_dump nextcloud > /tmp/backup-database-dump/dump.sql
          echo "... done"
        '';
        postHook = ''
          echo "Removing database dump"
          rm -rf /tmp/backup-database-dump

          echo "Disabling maintenance mode"
          /run/current-system/sw/bin/nextcloud-occ maintenance:mode --off
        '';
      };

      services.postgresql = {
        enable = true;
        dataDir = "/data/db";
        initialScript = pkgs.writeText "psql-init-script" ''
          create role nextcloud with login password 'CHANGE_THIS!';
          create database nextcloud with owner nextcloud;
        '';
      };

      services.nextcloud = {
        enable = true;
        hostName = "nextcloud.is.currently.online";
        nginx.enable = true;
        https = true;
        home = "/data/nextcloud-home";
        maxUploadSize = "10G";
        caching = {
          apcu = false;
          redis = true;
          memcached = false;
        };
        config = {
          adminpassFile = "/data/admin_password";
          extraTrustedDomains = [ "cloud.currently.online" ];
          dbtype = "pgsql";
          dbname = "nextcloud";
          dbuser = "nextcloud";
          dbhost = "localhost";
          dbport = 5432;
          dbpassFile = "/data/database_password";
        };
      };

      services.redis = {
        enable = true;
      };
    };
  };
}
