{ ... }:
{
  containers.revproxy = {
    autoStart = true;
    privateNetwork = true;

    hostAddress = "10.162.0.1";
    localAddress = "10.162.0.2";
    hostAddress6 = "2a01:4f8:120:614b:0:abc::1";
    localAddress6 = "2a01:4f8:120:614b:0:abc::2";

    config = { config, pkgs, lib, ... }: {
      networking.firewall.enable = false;

      environment.systemPackages = with pkgs; [
        traceroute dnsutils
      ];
      system.activationScripts.nginxCacheDir = ''
        mkdir -p /nginx-cache
        chown -R ${config.services.nginx.user}:${config.services.nginx.group} /nginx-cache
      '';

      nixpkgs.config.packageOverrides = super: {
        nginxMainline = super.nginxMainline.override { openssl = super.openssl_1_1; };

        prometheus-nginxlog-exporter =
          super.callPackage (
            import (builtins.fetchurl {
              url = "https://raw.githubusercontent.com/lschuermann/nur-packages/master/pkgs/prometheus-nginxlog-exporter/default.nix";
              sha256 = "1klgn90r68s5x58x4c60vhkvcwb5cdkpw93d2j2s2qr93w9gb2kv";
            })
          ) {};
      };

      systemd.services.prometheus-nginxlog-exporter =
        let
          exporterConfig = pkgs.writeText "prometheus-nginxlog-exporter-config.hcl" ''
            listen {
              port = 4040
              address = "[::]"
            }

            namespace "nginx_all" {
              format = "$remote_addr - $remote_user [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\" \"$http_x_forwarded_for\" $request_time $upstream_response_time"
              source_files = [
                "/var/spool/nginx/logs/access.log"
              ]
            }
          '';
        in {
          enable = true;
          wantedBy = [ "multi-user.target" ];
          after = [ "network.target" ];
          description = "nginx access.log-based prometheus exporter";
          serviceConfig = {
            Type = "simple";
            ExecStart = "${pkgs.prometheus-nginxlog-exporter}/bin/prometheus-nginxlog-exporter -config-file ${exporterConfig}";
            Restart = "always";
          };
        };

      systemd.services.nginx-logrotate =
        let
          rotateScript = pkgs.writeScript "nginx-rotate-log" ''
            #! ${pkgs.bash}/bin/bash

            if [ ! -d /var/spool/nginx/logs ]; then
              # No log directory, no logs to rotate
              exit 0
            fi

            cd /var/spool/nginx/logs

            [ -f access.log.2 ] && rm access.log.2
            [ -f access.log.1 ] && mv access.log.1 access.log.2
            [ -f access.log ] && mv access.log access.log.1

            # Logs have been rotated, tell nginx to reopen log files
            [ -e nginx.pid ] && kill -USR1 "$(cat nginx.pid)"
          '';
        in {
          description = "rotate the nginx access.log";
          # Start 15 minutes after every 8th hour past 2 AM
          startAt = "02/8:15";
          serviceConfig = {
            ExecStart = rotateScript;
            Type = "oneshot";
          };
        };

      services.nginx = {
        enable = true;
        package = pkgs.nginxMainline;

        recommendedProxySettings = true;
        recommendedTlsSettings = true;
        recommendedOptimisation = true;
        recommendedGzipSettings = true;

        sslProtocols = "TLSv1.2 TLSv1.3";
        sslCiphers = "EECDH+aRSA+AESGCM:EDH+aRSA:EECDH+aRSA:+AES256:+AES128:!SHA1:!CAMELLIA:!SEED:!3DES:!DES:!RC4:!eNULL";

        clientMaxBodySize = "10G";

        commonHttpConfig = ''
          # Default proxy cache location
          proxy_cache_path  /nginx-cache levels=1:2 keys_zone=STATIC:10m inactive=24h max_size=1g;

          # Specific log format to be read by the nginx prometheus log exporter
          log_format default '$remote_addr - $remote_user [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\" \"$http_x_forwarded_for\" $request_time $upstream_response_time';
          access_log logs/access.log default;

          # Add HSTS header with preloading to HTTPS requests.
          # Adding this header to HTTP requests is discouraged
          map $scheme $hsts_header {
              https   "max-age=31536000; includeSubdomains; preload";
          }
          add_header Strict-Transport-Security $hsts_header;
        '';

        virtualHosts."is.currently.online" = {
          enableACME = true;
          forceSSL = true;


          locations."/.well-known/matrix/" = {
            root = "/matrix-well-known";
          };
          

          #locations."/.well-known/matrix/client/" = {
          #  alias = pkgs.writeText "client" ''
          #    {
          #      "m.homeserver": {
          #        "base_url": "https://matrix.currently.online"
          #      },
          #      "m.identity_server": {
          #        "base_url": "https://vector.im"
          #      }
          #    }
          #  '';
          #  extraConfig = ''
          #    default_type application/json;
          #    add_header 'Access-Control-Allow-Origin' '*';
          #    add_header 'Access-Control-Allow-Methods' 'GET';
          #    add_header 'Access-Control-Max-Age' 1728000;
          #  '';
          #};

          #locations."/.well-known/matrix/server/" = {
          #  alias = pkgs.writeText "server" ''{"m.server":"matrix.currently.online:443"}'';
          #  extraConfig = ''
          #    default_type application/json;
          #  '';
          #};
        };

        virtualHosts."matrix.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://[2a01:4f8:120:614b:0:3a1::2]:8080";
          };
        };

        virtualHosts."web.git.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://10.162.3.2:80";
          };
        };

        virtualHosts."wekan.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://10.162.4.2:3000/";
          };
        };

        virtualHosts."cloud.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://[2a01:4f8:120:614b:0:c10::2]:80/";

            extraConfig = ''
              proxy_request_buffering off;
            '';
          };
        };

        virtualHosts."monitoring.cobalt.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://[2a01:4f8:120:614b:0:109:0:2]:61401/";
          };
        };

        virtualHosts."monitoring.carbon.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://[2a01:4f8:120:614b:1e0::2]:80/monitoring/";
          };
        };
        virtualHosts."ffsync.leons.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/" = {
            proxyPass = "http://[2a01:4f8:120:614b:1e0::2]:80/ffsyncserver/";
          };
        };

        virtualHosts."rapla-cache.currently.online" = {
          enableACME = true;
          forceSSL = true;

          locations."/stgtinf16a.ics" = {
            extraConfig = ''
              add_header            Allow "GET, HEAD" always;
              if ( $request_method !~ ^(GET|HEAD) ) {
                return 405;
              }

              proxy_pass            https://rapla.dhbw-stuttgart.de/rapla?key=2Y1jgykTdfXWACUIy5KDy9KiT2JmPu18tdTXD1BhB463zgJXL_rkmr_j6txk7L60;
              add_header            X-Cache-Status $upstream_cache_status;
              proxy_buffering       on;
              proxy_cache           STATIC;
              proxy_cache_key       $proxy_host$request_uri;
              proxy_cache_valid     200 1h;
              proxy_cache_use_stale error timeout invalid_header updating http_500 http_502 http_503 http_504;
              proxy_ignore_headers  X-Accel-Redirect X-Accel-Expires X-Accel-Limit-Rate X-Accel-Buffering X-Accel-Charset Expires Cache-Control Set-Cookie Vary;
            '';
          };
        };
      };
    };
  };
}
