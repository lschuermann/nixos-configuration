{ pkgs, ... }:

let

  simpleMirrorHookSrc = pkgs.fetchFromGitHub {
    owner = "miracle2k";
    repo = "gitolite-simple-mirror";
    rev = "b810ea2e";
    sha256 = "0a4y4997yvlik1h0hvhxq0gds5a2cj63p09j233n2mckq9wklgp8";
  };
  simpleMirrorHook = "${simpleMirrorHookSrc}/post-receive";

  cgitCustom  = pkgs.fetchFromGitHub {
    owner = "robyduck";
    repo = "cgit-custom";
    rev = "df9a4cb";
    sha256 = "0g2ianfmg6hsds9hzjm2fa5y8dlcd7aqm92niwp4rhn06a88sk8q";
  };

  highlightScript = ''
    #! ${pkgs.bash}/bin/bash
    # This script can be used to implement syntax highlighting in the cgit
    # tree-view by refering to this file with the source-filter or repo.source-
    # filter options in cgitrc.
    #
    # This script requires a shell supporting the ''${var##pattern} syntax.
    # It is supported by at least dash and bash, however busybox environments
    # might have to use an external call to sed instead.

    #
    # Code considered irrelevant snipped
    #

    # store filename and extension in local vars
    BASENAME="$1"
    EXTENSION="''${BASENAME##*.}"

    [ "''${BASENAME}" = "''${EXTENSION}" ] && EXTENSION=txt
    [ -z "''${EXTENSION}" ] && EXTENSION=txt

    # map Makefile and Makefile.* to .mk
    [ "''${BASENAME%%.*}" = "Makefile" ] && EXTENSION=mk

    # highlight versions 2 and 3 have different commandline options. Specifically,
    # the -X option that is used for version 2 is replaced by the -O xhtml option
    # for version 3.
    #
    # Version 2 can be found (for example) on EPEL 5, while version 3 can be
    # found (for example) on EPEL 6.
    #
    # This is for version 2
    #exec highlight --force -f -I -X -S "$EXTENSION" 2>/dev/null

    # This is for version 3
    exec ${pkgs.highlight}/bin/highlight --force --inline-css -f -I -O xhtml -S "$EXTENSION" 2>/dev/null
  '';


in
{
  containers.gitolite = {
    autoStart = true;
    privateNetwork = true;

    hostAddress = "10.162.3.1";
    localAddress = "10.162.3.2";
    hostAddress6 = "2a01:4f8:120:614b:0:917::1";
    localAddress6 = "2a01:4f8:120:614b:0:917::2";

    bindMounts = {
      "/data" = {
        hostPath = "/media/application-data/gitolite";
        isReadOnly = false;
      };
    };

    config = { config, pkgs, lib, ... }: {
      networking.firewall.enable = false;

      # Enable ssh for ssh-based git access
      services.openssh = {
        enable = true;
        ports = [ 22 ];
      };

      services.gitolite = {
        enable = true;
        dataDir = "/data/gitolite-home";
        # Only one initial key allowed here
        adminPubkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDf73AHm8dNnUl28R5PJ1S279v3pX4EnI+dAMjSgTZDK lschuermann/sulfur/github";
        user = "git";
        group = "git";

        commonHooks = [
          simpleMirrorHook
        ];

        extraGitoliteRc = ''
          $RC{'UMASK'} = 0027;
          $RC{'GIT_CONFIG_KEYS'} = '(gitolite\.mirror\..*|gitweb\..*)';
        '';
      };

      system.activationScripts."create_gitolite_home" = ''
        # Let's make sure we're creating the directory on the data mount,
        # never on the local root dir
        [ -d /data ] && mkdir -p /data/gitolite-home
      '';


      # Clone public repos via git://
      services.gitDaemon = {
        enable = true;
        basePath = "/data/gitolite-home/repositories";

        # Require the repository to be exported explicitly
        exportAll = false;

        user = "gitdaemon";
        group = config.services.gitolite.group;
      };
      users.extraUsers."${config.services.gitDaemon.user}" = {
        extraGroups = [ config.services.gitolite.group ];
      };
      users.groups.git.gid = lib.mkForce config.ids.gids.gitolite;

      # CGit webinterface
      services.lighttpd = {
        enable = true;

        cgit = {
          enable = false;
          subdir = "";

          configText = ''
            project-list=/data/gitolite-home/projects.list

            # Set title and description
            root-title=git.currently.online
            root-desc=cgit webinterface

            # Specify some default clone prefixes
            clone-prefix=ssh://git.currently.online git://git.currently.online https://web.git.currently.online
            snapshots=tar.gz zip

            readme=:README.md             # Display README.md files (located with your repos) in HTML (like github)
            about-filter=${pkgs.cgit}/lib/cgit/filters/about-formatting.sh

            source-filter=${ pkgs.writeScript "syntax-highlighting.sh" highlightScript }

            # define some common mime-types
            mimetype.git=image/git
            mimetype.html=text/html
            mimetype.jpg=image/jpeg
            mimetype.jpeg=image/jpeg
            mimetype.pdf=application/pdf
            mimetype.png=image/png
            mimetype.svg=image/svg+xml

            enable-index-links=1         # Show extra links for each repository on the index page
            enable-commit-graph=1        # Enable ASCII art commit history graph on the log pages
            enable-log-filecount=1
            enable-log-linecount=1
            enable-git-config=1          # Allow using gitweb.* keys

            # This needs to be the last configuration parameter for all others to take effect
            scan-path=/data/gitolite-home/repositories
          '';
        };


        # declare module dependencies
        enableModules = [ "mod_cgi" "mod_alias" "mod_setenv" ];

        extraConfig =
          let
            cfg = config.services.lighttpd.cgit;
            pathPrefix = if lib.stringLength cfg.subdir == 0 then "" else "/" + cfg.subdir;
          in ''
          $HTTP["url"] =~ "^/${cfg.subdir}" {
              cgi.assign = (
                  "cgit.cgi" => "${pkgs.cgit}/cgit/cgit.cgi"
              )
              alias.url = (
                  "${pathPrefix}/cgit.css" => "${cgitCustom}/cgit.css",
                  "${pathPrefix}/cgit.png" => "${pkgs.cgit}/cgit/cgit.png",
                  "${pathPrefix}/fedora-layout.css" => "${cgitCustom}/fedora-layout.css",
                  "${pathPrefix}/fedora-style.css" => "${cgitCustom}/fedora-style.css",
                  "${pathPrefix}/cgit-fedora.css" => "${cgitCustom}/cgit-fedora.css",
                  "${pathPrefix}/images/html-bg.png" => "${cgitCustom}/images/html-bg.png",
                  "${pathPrefix}/images/t.png" => "${cgitCustom}/images/t.png",
                  "${pathPrefix}"          => "${pkgs.cgit}/cgit/cgit.cgi"
              )
              setenv.add-environment = (
                  "CGIT_CONFIG" => "${pkgs.writeText "cgitrc" cfg.configText}"
              )
          }
        '';
      };
      #"${pathPrefix}/cgit.css" => "${cgitCustom}/cgit.css",
      #"${pathPrefix}/cgit.png" => "${pkgs.cgit}/cgit/cgit.png",
      #"${pathPrefix}"          => "${pkgs.cgit}/cgit/cgit.cgi"

      # make the cgitrc manpage available
      environment.systemPackages = [ pkgs.cgit ];
      systemd.services.lighttpd.preStart = ''
        mkdir -p /var/cache/cgit
        chown lighttpd:lighttpd /var/cache/cgit
      '';



      users.extraUsers."lighttpd".extraGroups =
        [ config.services.gitolite.group ];

    };
  };
}
