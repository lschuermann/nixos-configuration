{ ... }:

{
  containers.prometheus = {
    autoStart = true;
    privateNetwork = true;
    hostAddress6 = "2a01:4f8:120:614b:0:109::1";
    localAddress6 = "2a01:4f8:120:614b:0:109::2";

    config = { pkgs, ... }: {
      networking.firewall.enable = false;

      services.prometheus = {
        enable = true;
        extraFlags = [
          "-storage.local.retention 8760h"
          "-storage.local.series-file-shrink-ratio 0.3"
          "-storage.local.memory-chunks 2097152"
          "-storage.local.max-chunks-to-persist 1048576"
          "-storage.local.index-cache-size.fingerprint-to-metric 2097152"
          "-storage.local.index-cache-size.fingerprint-to-timerange 1048576"
          "-storage.local.index-cache-size.label-name-to-label-values 2097152"
          "-storage.local.index-cache-size.label-pair-to-fingerprints 41943040"
        ];
        listenAddress = "[::1]:61400";

        scrapeConfigs = [ {
          job_name = "node";
          scrape_interval = "10s";
          static_configs = [
            {
              targets = [
                "[2a01:4f8:120:614b:0:109:0:1]:61402"
              ];
              labels = {
                alias = "cobalt.is.currently.online";
              };
            }
            {
              targets = [
                "[2a01:4f8:120:614b:0:abc:0:2]:4040"
              ];
              labels = {
                alias = "proxy.cobalt.is.currently.online";
              };
            }
            {
              targets = [
                "[2a01:4f8:120:614b:c0d::2]:9137"
              ];
              labels = {
                alias = "leuschner.danielrutz.com";
              };
            }
          ];
        } ];
      };

      services.grafana = {
        enable = true;
        addr = "[::]";
        port = 61401;
        domain = "monitoring.currently.online";
        rootUrl = "https://monitoring.currently.online/";
      };
    };
  };

}

