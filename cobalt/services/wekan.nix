{ pkgs, ... }:

let
  nur = import ../../shared/nur.nix;
  nurPkgs = nur.global pkgs;
  nurModules = nur.globalNoPkgs;

in
{
  containers.wekan = {
    autoStart = true;
    privateNetwork = true;

    hostAddress = "10.162.4.1";
    localAddress = "10.162.4.2";
    hostAddress6 = "2a01:4f8:120:614b:0:f0c7::1";
    localAddress6 = "2a01:4f8:120:614b:0:f0c7::2";

    bindMounts = {
      "/data" = {
        hostPath = "/media/application-data/wekan";
        isReadOnly = false;
      };
    };

    config = { config, pkgs, ... }: {
      imports = [
        nurModules.repos.lschuermann.modules.wekan
      ];

      networking.firewall.enable = false;

      environment.systemPackages = with pkgs; [ mongodb-tools ];

      services = {
        mongodb = {
          enable = true;
          dbpath = "/data/db";
        };

        wekan = {
          enable = true;
          rootUrl = "https://wekan.currently.online/";
          host = "0.0.0.0";
          package = nurPkgs.repos.lschuermann.wekan;
          nodePackage = pkgs.nodejs-10_x;

          mail = {
            username = "wekan@is.currently.online";
            passwordFile = "/data/mail_password";
            host = "alkaid.uberspace.de";
            from = "wekan@is.currently.online";
          };
        };
      };

    };
  };
}
