{ ... }:
{
  containers.matrix = {
    autoStart = true;
    privateNetwork = true;

    hostAddress = "10.162.1.1";
    localAddress = "10.162.1.2";
    hostAddress6 = "2a01:4f8:120:614b:0:3a1::1";
    localAddress6 = "2a01:4f8:120:614b:0:3a1::2";

    bindMounts = {
      "/tmp/acme-cert-matrix.currently.online" = {
        hostPath = "/var/acme-cert-matrix.currently.online";
        isReadOnly = true;
      };
    };

    config = { config, pkgs, lib, ... }: {
      networking.firewall.enable = false;

      services.coturn = {
        enable = true;
        lt-cred-mech = true;
        use-auth-secret = true;
        # TODO: Change module to load this from file
        static-auth-secret = "";
        realm = "matrix.currently.online";
        relay-ips = [
          "2a01:4f8:120:614b:0:3a1::2"
          "10.162.1.2"
        ];
        listening-ips = [
          "2a01:4f8:120:614b:0:3a1::2"
          "10.162.1.2"
        ];
        no-tcp-relay = true;
        extraConfig = ''
          cipher-list="HIGH"
          no-loopback-peers
          no-multicast-peers
          external-ip=178.63.44.182/10.162.1.2
        '';
        secure-stun = true;
        cert = "/tmp/acme-cert-matrix.currently.online/fullchain.pem";
        pkey = "/tmp/acme-cert-matrix.currently.online/key.pem";
        tls-listening-port = 53049;
        min-port = 53100;
        max-port = 53999;
      };

      services.matrix-synapse = {
        enable = true;
        enable_registration = false;
        server_name = "is.currently.online";
        public_baseurl = "https://matrix.currently.online/";
        #tls_certificate_path = "/tmp/acme-cert-matrix.currently.online/fullchain.pem";
        no_tls = true;
        # TODO: Change module to load this from file
        turn_shared_secret = "";
        turn_uris = [
          "turn:nat.matrix.cobalt.is.currently.online:53049?transport=udp"
          "turn:nat.matrix.cobalt.is.currently.online:53050?transport=udp"
          "turn:nat.matrix.cobalt.is.currently.online:53049?transport=tcp"
          "turn:nat.matrix.cobalt.is.currently.online:53050?transport=tcp"
        ];
        listeners = [ {
          port = 8080;
          bind_address = "::";
          type = "http";
          tls = false;
          x_forwarded = true;
          resources = [{
            names = [ "federation" "client" "webclient" ];
            compress = false;
          }];
        } ];
        extraConfig = ''
          max_upload_size: "100M"
        '';
      };

      environment.systemPackages = [ pkgs.borgbackup ];
      services.borgbackup.jobs."matrix-synapse" =
      let dumpDir = "/root/backup-database-dump";
      in {
        paths = [ "/var/lib/matrix-synapse" dumpDir ];
        repo = "platinum-backup:.";
        archiveBaseName = "cobalt-matrix-synapse";
        startAt = "*-*-* 02:30:00"; # TODO: Randomize
        user = "root";
        group = "root";
        encryption = {
          mode = "repokey";
          passCommand = "cat /root/backup_passphrase";
        };
        compression = "auto,lzma";
        readWritePaths = [ dumpDir "/" ];
        privateTmp = false;
        doInit = false;
        preHook = ''
          echo "Dumping postgresql database"
          rm -rf ${dumpDir}
          mkdir -p ${dumpDir}
          ${pkgs.sudo}/bin/sudo -u postgres ${pkgs.postgresql}/bin/pg_dump matrix-synapse > ${dumpDir}/dump.sql
          echo "... done"
        '';
        postCreate = ''
          echo "Removing database dump"
          rm -rf ${dumpDir}
        '';
      };
      systemd.services."borgbackup-job-matrix-synapse" = {
        conflicts = [ "matrix-synapse.service" ];

        serviceConfig = {
          ExecStartPost = "${pkgs.systemd}/bin/systemctl start matrix-synapse.service";

          ProtectSystem = lib.mkForce null;
          PrivateTmp = lib.mkForce null;
          IOSchedulingClass = lib.mkForce null;
          CPUSchedulingPolicy = lib.mkForce null;
          ReadWritePaths = lib.mkForce null;
        };
      };

    };
  };
}
