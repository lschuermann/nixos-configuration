{ config, pkgs, lib, ... }:

with lib;

let
  nicConfig = {
    options = {
      type = lib.mkOption {
        description = "Virtual network adapter type";
        type = types.enum [ "bridge" ];
      };

      bridge = lib.mkOption {
        description = "Bridge to assign to (if type == bridge)";
        type = types.string;
      };
    };
  };

  volumeConfig = {
    options = {
      path = lib.mkOption {
        description = "Source path (applicable with type == qcow2 || block)";
        type = types.string;
      };

      type = lib.mkOption {
        description = "Volume type";
        type = types.enum [ "qcow2" "block" ];
        default = "qcow2";
      };

      bus = lib.mkOption {
        description = "VM volume bus type";
        type = types.enum [ "virtio" ];
        default = "virtio";
      };
    };
  };

  cdConfig = {
    options = {
      path = mkOption {
        description = "Path to an ISO image to be inserted as a virtual CD";
        type = types.path;
      };

      boot = mkOption {
        description = "Whether to try booting from cd";
        type = types.bool;
        default = false;
      };
    };
  };

  vmConfig = {
    options = {
      enable = lib.mkOption {
        description = "Enable autostart for this virtual machine";
        type = types.bool;
        default = false;
      };

      cores = lib.mkOption {
        description = "VM core count";
        type = types.ints.unsigned;
        default = 1;
      };
      
      memory = lib.mkOption {
        description = "VM RAM assignment in megabytes";
        type = types.ints.unsigned;
        default = 1024;
      };

      nics = lib.mkOption {
        description = "VM network interfaces";
        type = with types; attrsOf (submodule nicConfig);
        default = {};
      };

      volumes = lib.mkOption {
        description = "VM volumes";
        type = with types; attrsOf (submodule volumeConfig);
        default = {};
      };

      uefi = lib.mkOption {
        description = "Use UEFI (OVMF) or BIOS";
        type = types.bool;
        default = false;
      };

      cd = mkOption {
        description = "Virtual CD";
        type = with types; nullOr (submodule cdConfig);
        default = null;
      };
    };
  };

  cfg = config.services.virtualMachines;

in
{

  options.services.virtualMachines = lib.mkOption {
    description = "Declaratively defined virtual machines";
    type = with types; attrsOf (submodule vmConfig);
    default = {};
  };

  config = lib.mkIf ((builtins.length (builtins.attrNames cfg)) > 0) {
    virtualisation.libvirtd.enable = true;

    systemd.services = lib.mapAttrs' (name: guest: lib.nameValuePair "libvirtd-guest-${name}" {
      enable = guest.enable;
      after = [ "libvirtd.service" ];
      requires = [ "libvirtd.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      script =
        let
          xml = pkgs.writeText "libvirt-guest-${name}.xml"
            ''
              <domain type="kvm">
                <name>${name}</name>

                <uuid>UUID</uuid>

                <os>
                  <type ${ if guest.uefi then "machine='pc-q35-3.0'" else "" }>hvm</type>
                  <boot dev="${ if guest ? cd && !(isNull guest.cd) && guest.cd.boot then "cdrom" else "hd" }" />

                  ${ if guest.uefi then ''
                    <loader readonly='yes' type='pflash'>${pkgs.OVMF.fd}/FV/OVMF_CODE.fd</loader>
                    <nvram template='${pkgs.OVMF.fd}/FV/OVMF_VARS.fd'>/var/lib/libvirt/qemu/nvram/${name}_VARS.fd</nvram>
                  '' else "" }
                </os>

                <vcpu mode="host-passthrough">${toString guest.cores}</vcpu>
                <cpu mode='host-passthrough'>
                  <cache mode='passthrough'/>
                  <feature policy='disable' name='lahf_lm'/>
                </cpu>

                <memory unit="MiB">${toString guest.memory}</memory>

                <devices>

                  <!--<filesystem type="mount" accessmode="mapped">
                    <source dir="/media/nasvol0/lead" />
                    <target dir="nasvol0-lead" />
                  </filesystem>-->

                  <!-- #################### VOLUMES #################### -->
                  ${ builtins.concatStringsSep "\n" (
                    lib.mapAttrsToList (target: volume:
                      # For each volume

                      if volume.type == "block" then ''
                        <!-- VOLUME => type = "block", target = "${ target }", src = "${ volume.path }", bus = "${ volume.bus }" -->
                        <disk type="block" device="disk">
                          <source dev="${ volume.path }" />
                          <target dev="${ target }" bus="${ volume.bus }" />
                        </disk>

                      '' else if volume.type == "qcow2" then ''
                        <!-- VOLUME => type = "qcow2", target = "${ target }", src = "${ volume.path }", bus = "${ volume.bus }" -->
                        <disk type="file" device="disk">
                          <driver name="qemu" type="qcow2" />
                          <source file="${ volume.path }" />
                          <target dev="${ target }" bus="${ volume.bus }" />
                        </disk>

                      '' else ''
                        <!-- ERROR: invalid disk type in NixOS config "${ volume.type }" -->

                      ''
                    ) guest.volumes
                  ) }

                  ${ if guest ? cd && !(isNull guest.cd) then ''
                    <disk type="file" device="cdrom">
                      <driver name="qemu" type="raw"/>
                      <source file="${guest.cd.path}"/>
                      <target dev="hdb" bus="${ if guest.uefi then "sata" else "ide" }"/>
                      <readonly/>
                      <address type="drive" controller="0" bus="0" target="0" unit="1"/>
                    </disk>
                    ${ if guest.uefi then
                      ''<controller type="sata" index="0" />''
                    else
                      ''<controller type="ide" index="0" />''
                    }
                  '' else "" }

                  <!-- #################### NETWORK INTERFACES #################### -->
                  ${ builtins.concatStringsSep "\n" (
                    lib.mapAttrsToList (mac: nic:
                      if nic.type == "bridge" then ''
                        <!-- NIC => mac = "${ mac }", type = "bridge", bridge = "${ nic.bridge }" -->
                        <interface type="bridge">
                          <source bridge="${ nic.bridge }" />
                          <mac address="${ mac }"/>
                          <model type="virtio"/>
                        </interface>

                      '' else ''
                        <!-- ERROR: invalid NIC type in NixOS config "${ nic.type }" -->

                      ''
                    ) guest.nics
                  ) }

                  <!-- #################### SERIAL CONSOLE #################### -->
                  <console type='pty'>
                    <target type='serial' port='0'/>
                  </console>

                  <!-- #################### SPICE VIDEO #################### -->
                  ${ if guest ? spice && !(isNull guest.spice) then ''
                    <video>
                      <model type='qxl'/>
                    </video>
                    <graphics
                        type="spice"
                        ${ if guest.spice ? password && !(isNull guest.spice.password) then ''passwd="${ guest.spice.password }"'' else "" }
                        ${ if guest.spice ? port && !(isNull guest.spice.port) then ''autoport="no" port="${ toString guest.spice.port }"'' else ''autoport="yes"'' }
                        listen="${ if guest.spice ? listenAddress && !(isNull guest.spice.listenAddress) then guest.spice.listenAddress else "127.0.0.1" }">
                      <listen type="address" address="${ if !(isNull guest.spice.listenAddress) then guest.spice.listenAddress else "127.0.0.1" }" />
                    </graphics>
                    <input type="keyboard" bus="usb"/>
                    <controller type='virtio-serial' index='0'/>
                    <channel type='spicevmc'>
                      <target type='virtio' name='com.redhat.spice.0'/>
                    </channel>
                  '' else "" }

                </devices>

                <features>
                  <acpi/>
                </features>

              </domain>
            '';
        in
          ''
            uuid="$(${pkgs.libvirt}/bin/virsh domuuid '${name}' || true)"
            ${pkgs.libvirt}/bin/virsh define <(sed "s/UUID/$uuid/" '${xml}')
            ${pkgs.libvirt}/bin/virsh start '${name}'
          '';
      preStop =
        ''
          ${pkgs.libvirt}/bin/virsh shutdown '${name}'
          let "timeout = $(date +%s) + 10"
          while [ "$(${pkgs.libvirt}/bin/virsh list --name | grep --count '^${name}$')" -gt 0 ]; do
            if [ "$(date +%s)" -ge "$timeout" ]; then
              # Meh, we warned it...
              ${pkgs.libvirt}/bin/virsh destroy '${name}'
            else
              # The machine is still running, let's give it some time to shut down
              sleep 0.5
            fi
          done
        '';
    }) cfg;
  };

}
