# Function to decrypt passwords which shouldn't be stored unencrypted in
# public locations (such as a GIT repository)

pkgs: keyFile: name: ciphertext:
  pkgs.lib.removeSuffix "\n" (
    builtins.readFile (
      pkgs.runCommand name {} ''
        echo "${ciphertext}" | \
        ${pkgs.openssl}/bin/openssl enc -aes-256-cbc -a -d -kfile "${keyFile}" -out "$out"
      ''
    )
  )
